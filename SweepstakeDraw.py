#!/usr/bin/env python
import os,sys,subprocess,datetime,copy,math,shutil,string, time, random
__doc__    = """Script to assign things randomly
"""

teamPeopleDict = dict()
teamPeopleDict1 = dict()
teamPeopleDict2 = dict()
teamPeopleDict3 = dict()
teamPeopleDict4 = dict()



teamListOne = ["Germany","Brazil","Portugal","Argentina","Belgium","Switzerland","France","Spain"]
teamListTwo = ["Poland","Denmark","England","Tunisia","Mexico","Colombia","Uruguay","Croatia"]
teamListThree = ["Iceland","Sweden","Costa Rica","Senegal","Serbia","Iran","Australia","Morocco"]
teamListFour = ["Russia","Egypt","Saudi Arabia","Peru","Nigeria","South Korea","Panama","Japan"] 

peopleListOne = ["James","Joe","Dan","Connor","Glenn","Harry","John"]
peopleListTwo=["James","Joe","Dan","Connor","Glenn","Harry","John"]
peopleListThree=["James","Joe","Dan","Connor","Glenn","Harry","John"]
peopleListFour=["James","Joe","Dan","Connor","Glenn","Harry","John"]

print "Selecting Teams"
os.system('say -r 160 Selecting Teams')

while (len(peopleListOne) > 0):
    team = random.choice(teamListOne)
    person = random.choice(peopleListOne)
    teamPeopleDict1.update({person: team})    
    if team in teamListOne: teamListOne.remove(team)
    if person in peopleListOne: peopleListOne.remove(person)

while (len(peopleListTwo) > 0):
    team = random.choice(teamListTwo)
    person = random.choice(peopleListTwo)
    teamPeopleDict2.update({person: team})    
    if team in teamListTwo: teamListTwo.remove(team)
    if person in peopleListTwo: peopleListTwo.remove(person)

while (len(peopleListThree) > 0):
    team = random.choice(teamListThree)
    person = random.choice(peopleListThree)
    teamPeopleDict3.update({person: team})    
    if team in teamListThree: teamListThree.remove(team)
    if person in peopleListThree: peopleListThree.remove(person)

while (len(peopleListFour) > 0):
    team = random.choice(teamListFour)
    person = random.choice(peopleListFour)
    teamPeopleDict4.update({person: team})    
    if team in teamListFour: teamListFour.remove(team)
    if person in peopleListFour: peopleListFour.remove(person)


print "Bottom tier teams"
os.system('say -r 160 Bottom tier teams')
while (len(teamPeopleDict4) > 0):
      name,team = random.choice(list(teamPeopleDict4.items()))
      print name, ":", team
      os.system('say -r 160 '+name+" selected "+team)
      if name in teamPeopleDict4.keys(): del teamPeopleDict4[name]

print "Third tier teams"
os.system('say -r 160 Third tier teams')
while (len(teamPeopleDict3) > 0):
      name,team = random.choice(list(teamPeopleDict3.items()))
      print name, ":", team
      os.system('say -r 160 '+name+" selected "+team)
      if name in teamPeopleDict3.keys(): del teamPeopleDict3[name]

print "Second tier teams"
os.system('say -r 160 Second tier teams')
while (len(teamPeopleDict2) > 0):
      name,team = random.choice(list(teamPeopleDict2.items()))
      print name, ":", team
      os.system('say -r 160 '+name+" selected "+team)
      if name in teamPeopleDict2.keys(): del teamPeopleDict2[name]

print "Top tier teams"
os.system('say -r 160 Top tier teams')
while (len(teamPeopleDict1) > 0):
      name,team = random.choice(list(teamPeopleDict1.items()))
      print name, ":", team
      os.system('say -r 160 '+name+" selected "+team)
      if name in teamPeopleDict1.keys(): del teamPeopleDict1[name]





teamList = teamListOne + teamListTwo + teamListThree + teamListFour
teamsLeft = str(len(teamList))
print teamsLeft, "teams remaining"
os.system('say -r 160 '+teamsLeft+" teams remaining ")
tc = 1
for i in teamList:
    if tc==len(teamList): os.system('say -r 90 '+"and")
    print i  
    os.system('say -r 160 '+i)
    tc+=1